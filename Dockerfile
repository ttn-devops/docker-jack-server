FROM ubuntu

RUN apt-get update && apt-get install jackd -y

COPY start.sh /usr/local/bin/start.sh

CMD ["start.sh"]
